from typing import Optional, AnyStr
from urllib.parse import urlparse, parse_qs, unquote


class OTP:
    def __init__(
        self, issuer: str, account: str, secret: str, algorithm: str, digits: int,
    ):
        self.issuer = issuer
        self.account = account
        self.secret = secret
        self.__algorithm = None
        self.algorithm = algorithm
        self.__digits = None
        self.digits = digits

    @property
    def algorithm(self):
        return self.__algorithm

    @algorithm.setter
    def algorithm(self, value):
        allowedAlgoritmhs = ["SHA1", "SHA256", "SHA512"]
        if value not in allowedAlgoritmhs:
            raise ValueError("Valid values for algorithm: {}".format(allowedAlgoritmhs))
        else:
            self.__algorithm = value

    @property
    def digits(self):
        return self.__digits

    @digits.setter
    def digits(self, value):
        if value not in [6, 8]:
            raise ValueError("Number of digits must be 6 or 8")
        else:
            self.__digits = value

    def __eq__(self, other):
        if not isinstance(other, type(self)):
            return NotImplemented

        return all([getattr(other, k) == v for (k, v) in self.__dict__.items()])

    def __repr__(self):
        return "{}({})".format(
            self.__class__.__name__,
            ", ".join("{}: {}".format(k, v) for k, v in self.__dict__.items()),
        )


class TOTP(OTP):
    def __init__(
        self,
        issuer: str,
        account: str,
        secret: str,
        algorithm: str = "SHA1",
        digits: int = 6,
        period: int = 30,
    ):
        super().__init__(issuer, account, secret, algorithm, digits)
        self.__period = None
        self.period = period

    @property
    def period(self):
        return self.__period

    @period.setter
    def period(self, value):
        if value < 0:
            raise ValueError(
                "Period shall be non-negative. Traveling back in time is not allowed!"
            )
        else:
            self.__period = value


class HOTP(OTP):
    def __init__(
        self,
        issuer: str,
        account: str,
        secret: str,
        counter: int,
        algorithm: str = "SHA1",
        digits: int = 6,
    ):
        super().__init__(issuer, account, secret, algorithm, digits)
        self.__counter = None
        self.counter = counter

    @property
    def counter(self):
        return self.__counter

    @counter.setter
    def counter(self, value):
        if not isinstance(value, int):
            raise TypeError("Counter must be an integer number")
        else:
            self.__counter = value


def fromURI(uri: str) -> OTP:
    if not isinstance(uri, str):
        raise TypeError("URI must be a string")

    components = urlparse(uri)

    def int_None(a: Optional[AnyStr]) -> Optional[AnyStr]:
        """
        Convert input string like object to integer if not None.
        If input is None, output will be None
        """
        if a is None:
            return None
        else:
            return int(a)

    args = {}

    label = components.path.strip("/").split(":")
    parameters = parse_qs(components.query)

    args["account"] = label[-1]
    args["issuer"] = unquote(parameters.get("issuer", [""])[0])
    # Only retreive issuer from label when not explicitly given by parameters
    if (len(label) == 2) and (args["issuer"] == ""):
        args["issuer"] = unquote(label[0])

    args["secret"] = parameters["secret"][0]
    args["algorithm"] = parameters.get("algorithm", [None])[0]
    args["digits"] = int_None(parameters.get("digits", [None])[0])
    args["period"] = int_None(parameters.get("period", [None])[0])
    args["counter"] = int_None(parameters.get("counter", [None])[0])

    # Remove all None arguments to let the constructor decide the default values
    for (k, v) in list(args.items()):
        if v is None:
            del args[k]

    otp_type = components.netloc.lower()
    if otp_type == "totp":
        return TOTP(**args)
    elif otp_type == "hotp":
        return HOTP(**args)
