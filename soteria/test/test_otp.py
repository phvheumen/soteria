import unittest
from typing import Optional
from soteria import otp


def simple_totp_instant():
    return otp.TOTP(
        issuer="Amazon",
        account="john.doe@example.com",
        secret="HXDMVJECJJWSRB3HWIZR4IFUGFTMXBOZ",
    )


def simple_hotp_instant():
    return otp.HOTP(
        issuer="Amazon",
        account="john.doe@example.com",
        secret="HXDMVJECJJWSRB3HWIZR4IFUGFTMXBOZ",
        counter=12345,
    )


class testOTPClass(unittest.TestCase):
    # Default instantiation values
    def test_totp_default_period(self):
        entry = simple_totp_instant()
        self.assertEqual(entry.period, 30)

    def test_totp_default_digits(self):
        entry = simple_totp_instant()
        self.assertEqual(entry.digits, 6)

    def test_totp_default_algorithm(self):
        entry = simple_totp_instant()
        self.assertEqual(entry.algorithm, "SHA1")

    def test_hotp_default_digits(self):
        entry = simple_hotp_instant()
        self.assertEqual(entry.digits, 6)

    def test_hotp_default_algorithm(self):
        entry = simple_hotp_instant()
        self.assertEqual(entry.algorithm, "SHA1")

    # Attribute limits
    def test_throw_on_negative_period(self):
        with self.assertRaises(ValueError):
            entry = simple_totp_instant()
            entry.period = -1

    def test_throw_on_invalid_digits(self):
        with self.assertRaises(ValueError):
            entry = simple_totp_instant()
            entry.digits = 4

    def test_throw_on_invalid_algorithm(self):
        with self.assertRaises(ValueError):
            entry = simple_totp_instant()
            entry.algorithm = "Not valid algortihm"

    def test_throw_on_non_integer_counter(self):
        with self.assertRaises(TypeError):
            entry = simple_hotp_instant()
            entry.counter = "12345"


class testURICreation(unittest.TestCase):
    def test_throw_on_non_string(self):
        with self.assertRaises(TypeError):
            otp.fromURI(123)

    def test_minimal_uri(self):
        entry = otp.fromURI(
            "otpauth://totp/john.doe@email.com?secret=HXDMVJECJJWSRB3HWIZR4IFUGFTMXBOZ"
        )
        expected_entry = otp.TOTP(
            issuer="",
            account="john.doe@email.com",
            secret="HXDMVJECJJWSRB3HWIZR4IFUGFTMXBOZ",
        )
        self.assertEqual(entry, expected_entry)

    def test_issuer_from_label(self):
        entry = otp.fromURI(
            "otpauth://totp/ACME%20Co:john.doe@email.com?secret=HXDMVJECJJWSRB3HWIZR4IFUGFTMXBOZ"
        )
        expected_entry = otp.TOTP(
            issuer="ACME Co",
            account="john.doe@email.com",
            secret="HXDMVJECJJWSRB3HWIZR4IFUGFTMXBOZ",
        )
        self.assertEqual(entry, expected_entry)

    def test_issuer_from_parameter(self):
        entry = otp.fromURI(
            "otpauth://totp/john.doe@email.com?secret=HXDMVJECJJWSRB3HWIZR4IFUGFTMXBOZ&issuer=ACME%20Co"
        )
        expected_entry = otp.TOTP(
            issuer="ACME Co",
            account="john.doe@email.com",
            secret="HXDMVJECJJWSRB3HWIZR4IFUGFTMXBOZ",
        )
        self.assertEqual(entry, expected_entry)

    def test_prefer_issuer_from_parameter(self):
        entry = otp.fromURI(
            "otpauth://totp/ACME%20Co%20Label:john.doe@email.com?secret=HXDMVJECJJWSRB3HWIZR4IFUGFTMXBOZ&issuer=ACME%20Co"
        )
        expected_entry = otp.TOTP(
            issuer="ACME Co",
            account="john.doe@email.com",
            secret="HXDMVJECJJWSRB3HWIZR4IFUGFTMXBOZ",
        )
        self.assertEqual(entry, expected_entry)

    def test_full_uri(self):
        entry = otp.fromURI(
            "otpauth://totp/ACME%20Co:john.doe@email.com?secret=HXDMVJECJJWSRB3HWIZR4IFUGFTMXBOZ&issuer=ACME%20Co&algorithm=SHA256&digits=8&period=20"
        )
        expected_entry = otp.TOTP(
            issuer="ACME Co",
            account="john.doe@email.com",
            secret="HXDMVJECJJWSRB3HWIZR4IFUGFTMXBOZ",
            algorithm="SHA256",
            digits=8,
            period=20,
        )
        self.assertEqual(entry, expected_entry)

    def test_hotp_minimal_uri(self):
        entry = otp.fromURI(
            "otpauth://hotp/john.doe@email.com?secret=HXDMVJECJJWSRB3HWIZR4IFUGFTMXBOZ&counter=12345"
        )
        expected_entry = otp.HOTP(
            issuer="",
            account="john.doe@email.com",
            secret="HXDMVJECJJWSRB3HWIZR4IFUGFTMXBOZ",
            counter=12345,
        )
        self.assertEqual(entry, expected_entry)

    # Tests
    # - Cases with incomplete required information
    # - invalid URI


if __name__ == "__main__":
    unittest.main()
