import qrcode
import base64
from numpy.random import Generator, PCG64
import os

rg = Generator(PCG64(list(base64.b64decode("JU9Hp4tjjfcia7+GCUFHTQ=="))))
S = list(
    "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890-._~:/?#[]@!$&'()*+,;%="
)


N = 99999
for k in range(0, N):
    qr = qrcode.QRCode(
        version=None,
        error_correction=qrcode.constants.ERROR_CORRECT_M,
        box_size=10,
        border=0,
    )
    sample = rg.choice(S, size=rg.integers(32, 256), replace=True)
    random_string = "".join(sample)
    qr.add_data(random_string)
    qr.make(fit=True)
    img = qr.make_image()
    img.save("qr_codes/{:05}.png".format(k))
    with open("qr_codes/{:05}.txt".format(k), "w") as f:
        f.write(random_string)

    if not k % 100:
        print("{}/{}".format(k, N))
