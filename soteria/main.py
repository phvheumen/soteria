import jinja2
import base64
from soteria import otp
import cv2
import numpy as np


def run():

    with open("download.png", "rb") as img:
        d = img.read()

    img = cv2.imread("download.png", cv2.IMREAD_GRAYSCALE)
    print(img.shape)
    print(img.dtype)
    img = np.pad(img, (20, 20), constant_values=255)
    cv2.imshow("QR code", img)
    cv2.waitKey(0)
    qr = cv2.QRCodeDetector()
    (qr_data, qr_bb, _) = qr.detectAndDecode(img)

    print(qr_bb)
    print(qr_data)

    loader = jinja2.PackageLoader("soteria", "templates")
    env = jinja2.Environment(loader=loader, autoescape=True)
    template = env.get_template("report_2fa.html")
    entry = otp.fromURI(qr_data)
    print(entry)

    otp_parameters = {}
    otp_parameters["type"] = "TOTP"
    otp_parameters["label"] = entry.issuer
    otp_parameters["secret"] = entry.secret
    otp_parameters["issuer"] = entry.issuer
    otp_parameters["account"] = entry.account
    otp_parameters["algorithm"] = entry.algorithm
    otp_parameters["digits"] = entry.digits
    otp_parameters["counter"] = None
    otp_parameters["period"] = entry.period
    otp_parameters["recovery_codes"] = [
        "JBSWY3DPEHPK3PXP",
        "JBSWY3DPEHPK3PXP",
        "JBSWY3DPEHPK3PXP",
    ]
    otp_parameters["qr_code"] = base64.b64encode(d).decode("utf-8")

    html_out = template.render(otp_parameters)

    with open("report.html", "w") as f:
        f.write(html_out)

    # otp.parse()

